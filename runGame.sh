#!/bin/bash
mkdir -p build
rm -f build/*
g++ -std=c++11 MyBot.cpp -o build/MyBot.o
g++ -std=c++11 MyOldBot.cpp -o build/MyOldBot.o
g++ -std=c++11 RandomBot.cpp -o build/RandomBot.o
./halite -d "35 35" "build/MyBot.o" "build/MyBot.o"
