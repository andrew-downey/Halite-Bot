#include <stdlib.h>
#include <time.h>
#include <cstdlib>
#include <ctime>
#include <time.h>
#include <set>
#include <fstream>
#include <algorithm>

#include "hlt.hpp"
#include "networking.hpp"

#undef min

unsigned char myID;
hlt::GameMap gameMap;

int findNearestEnemyDirection(hlt::Location loc) {
	int direction = NORTH;
	// don't get stuck in an infinite loop
	float maxDistance = std::min(gameMap.width, gameMap.height) / 2;

	for(int i = 1; i < 5; i++) {
		int distance = 0;
		hlt::Location current = loc;
		hlt::Site site = gameMap.getSite(current, i);
		while (site.owner == myID && distance < maxDistance) {
			distance++;
			current = gameMap.getLocation(current, i);
			site = gameMap.getSite(current);
		}

		if (distance < maxDistance) {
			direction = i;
			maxDistance = distance;
		}
	}

	return direction;
}

int main() {
	srand(time(NULL));

	std::cout.sync_with_stdio(0);

	getInit(myID, gameMap);
	sendInit("AnooBot v3");

	std::set<hlt::Move> moves;
	hlt::Site site, testSite;
	while(true) {
		moves.clear();

		getFrame(gameMap);

		for(unsigned short a = 0; a < gameMap.height; a++) {
			for(unsigned short b = 0; b < gameMap.width; b++) {
				site = gameMap.getSite({ b, a });
				if (site.owner == myID) {
					unsigned char direction = STILL;
					bool border = false;
					for(int i = 1; i < 5; i++) {
						testSite = gameMap.getSite({b, a}, i);
						if(testSite.owner != myID && testSite.strength < site.strength) {
							direction = i;
							border = true;
							break;
						}
					}

					if(site.strength < (site.production * 7)) {
						direction = STILL;
					}else if (!border) {
						direction = findNearestEnemyDirection({ b, a });
					}

					if(gameMap.getSite({ b, a }, direction).strength > site.strength) {
						direction = STILL;
					}
					moves.insert({ { b, a }, direction });
				}
			}
		}

		sendFrame(moves);
	}

	return 0;
}
